# README Spartakus Framework #

![Was mit Spartakus geht.](Dokumentation/Beispiel_0.png "Was mit Spartakus geht.")

### System ###

Auf Basis eines Protokolls auf MQTT Basis sprechen diverse Entitäten in einem Netzwerk miteinander. Entitäten können hier sowohl Hardwareobjekte sein (WEMOS/ARDUINO Microcontroller mit angeschlossenen Sensoren/Aktoren), fertige IoT Lösungen (Philips HUE, etc.), oder Softwareanwendung (Twitter, Python Skripte, Processing Sketche, Telegram Messenger, etc.). Diese Entitäten sind als Knotenpunkte (Nodes) gedacht. Ihre In- und Outputs können miteinander verknüpft werden.


### Logic ###

![Was mit Spartakus geht.](Dokumentation/System_Aufbau.png "Was mit Spartakus geht.")


Das hier vorliegende _Logik Skript_ ist die Kommunikationszentrale des Systems. Es registriert und speichert die Zustände aller Entitäten im Netzwerk und erstellten Verknüpfungen/Regeln in einer Rethink Datenbank.
Zunächst erfasst die Logik auf Basis des Protokolls alle im Netzwerk vorhandenen Entitäten. Sobald eine Softwareanwendung oder ein vernetztes Gerät im lokalen Netzwerk angemeldet ist, registriert die Logik dies und erzeugt einen neuen Eintrag in einer Datenbank mit Informationen zum Zustand,dem Namen und der Art der Ein- und Ausgängen, die sie bei der Entität erfragt. Außerdem werden die zur Kommunikation mit der Entität nötigen Informationen/Adressen gespeichert.
Ändert sich der Zustand einer Entität, protokolliert dies die Logik und leitet die Information darüber, je nach Regel, weiter.

* Version 0.1
* Lizenz: MIT
* Python3 Skript entstanden im Rahmen der Masterarbeit "web of entities" im Interaction Design an der Hochschule Magdeburg-Stendal.

### Protokoll ###

Damit alle Teilnehmer im Netzwerk miteinander kommunizieren können wurde ein Protokoll entwickelt, welches auf MQTT aufbaut.
Über sogenannte Topixs werden Nachrichten adressiert. Jeder Teilnehmer im Netzwerk kann beliebige Topics abonnieren. Das Logic Skript verwaltet die Abos.
Das Protokoll ist im PDF "web of entities prototyp" genauer dokumentiert.

![Was mit Spartakus geht.](Dokumentation/Protokoll.png "Was mit Spartakus geht.")


```
/spartacus/thing/PHILIPS_HUE1_BAD/output/status
/spartacus/thing/PHILIPS_HUE1_BAD/input/brightness

/spartacus/view/PROCESSING_GUI/output/rule/add
/spartacus/view/PROCESSING_GUI/input/thing/remove
```
Beispiele für Topics

### How do I get set up? ###

* Python 3 installieren
* Python Dependencies installieren: paho.mqtt.client, rethinkdb, json, time ``` pip3 install paho-mqtt rethinkdb```
* einen beliebigen MQTT-Broker im Netzwerk starten ``` Defaults: MQTTP_HOST = '192.168.0.100' MQTTP_PORT = 1883```
* RethinkDB installieren und starten ``` Defaults DB_HOST = 'localhost' DB_PORT = 28015```
* ``` python3 logic.py ```


### Weitere Tools für das Spartakus Framework ###

* [View](https://bitbucket.org/bene_kaffai/spartakus-view) (grafische Nutzer*Innen-Oberfläche zum nodebasierten erstellen und verwalten von Interkationen zwischen Entitäten)
* [Logic Tings](https://bitbucket.org/bene_kaffai/spartakus-logic-thing) (einfache Logikbausteine AND, OR, COMPARE, etc. um Entitäten komplex miteinander zu verknüpfen)
* [WEMOS](https://bitbucket.org/bene_kaffai/spartakus-wemos-template) und [PYTHON](https://bitbucket.org/bene_kaffai/spartakus-python-template) Templates (um eigene Entitäten im Netzwerk verfügbar zu machen)

### Beispiel ###

![Was mit Spartakus geht.](Dokumentation/Beispiel_1.png "Was mit Spartakus geht.")
![Was mit Spartakus geht.](Dokumentation/Beispiel_2.png "Was mit Spartakus geht.")


### Who do I talk to? ###

* Repo owner or admin: bene_kaffai
