#!/bin/bash

gnome-terminal --geometry 90x10+1000+880 -e 'rethinkdb --config-file /etc/rethinkdb/default.conf'
gnome-terminal --geometry 90x10+1000+880 -e 'rethinkdb restore --force /home/bene/Dokumente/Spartakus/rethinkdb/cebit_default.tar.gz'
gnome-terminal --geometry 90x15+1000+0 -e   'python3 /home/bene/Dokumente/Spartakus/spartakus-werkzeugkoffer/mqtt_pub_sub_debug.py'

sleep 3

gnome-terminal --geometry 90x15+1000+340 -e       'python3 /home/bene/Dokumente/Spartakus/spartakus-logic/logic.py'


