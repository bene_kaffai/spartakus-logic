#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# by: bene & ben
#
# pip3 install rethinkdb paho-mqtt tornado
#

import paho.mqtt.client as mqtt
import rethinkdb as r
import json
import time

from queue import Queue
from threading import Thread

'''
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.template
'''

##############################################################################
# GLOBAL VARIABLES
##############################################################################

MQTTP_HOST = '192.168.0.100'  # 'broker.hivemq.com'
MQTTP_PORT = 1883
DB_HOST = 'localhost'
DB_PORT = 28015

CLIENTID = 'LOGIC'

NETWORK = '/spartakus/thing/'
TOPIC_OUT_DB = '/spartakus/db/' + CLIENTID + '/output/'
TOPIC_OUT = NETWORK + CLIENTID + '/output/'
TOPIC_IN = NETWORK + CLIENTID + '/input/'
SUBSCRIBE_THINGS = NETWORK + '+/output/#'
TOPIC_PING = NETWORK + 'all/input/ping'

# Json with all I/O infos about this node
REPORT_IO = json.dumps({'name': CLIENTID, 'status': 1, 'type': 'logic',
                        'input': {'rule': 1, 'thing': 'name', 'report': 1, 'change': 1, 'dump': 1}, 'output': {'rule': 'json', 'change': 'json', 'dump': 'json', 'report': 'json'}})

THINGS = {}
TIME = "Szene1"

##############################################################################
# HELPER FUNCTIONS
##############################################################################


def send_mqtt(topic, payload):
    client.publish(topic, payload)


def build_topic(thing, argument):
    topic = NETWORK + thing + "/input/" + argument
    return topic


# returns matching Dictonary Key (callback) to topic
def get_matching(topic):
    for key in mqtt_callback.keys():
        if topic.startswith(key):
            return key


def get_thing(topic):
    return topic.split(NETWORK)[1].split('/')[0]
    # return filter(None, msg.topic.split('/'))
    # return [x for x in msg.topic.split('/') if x]


def get_argument(topic):
    return topic.split(NETWORK)[1].split('/')[-1]


##############################################################################
# MQTT CALLBACK
##############################################################################


# default callback if no specific topic is registered
def callback_logic(thing, argument, message):
    #  update value (payload) of thing/output/argument in db and check for rules and send
    if thing in THINGS and argument not in ['report', 'status']:
        r.table("things").get(THINGS[thing]).update({"output": {argument: message}}).run(spartakus)
        print('updated %s in DB' % thing)  # update value of thing in DB

        rules = r.table("rules").filter({"sender": {"name": thing, "output": argument}}).pluck("receiver").run(spartakus)
        for rule in rules:
            send_mqtt(build_topic(rule['receiver']['name'], rule['receiver']['input']), message)
            print('found rule in db for %s' % str(rule['receiver']['name']))

    elif thing in THINGS and argument == 'status':
        r.table('things').get(THINGS[thing]).update({argument: message}).run(spartakus)
        print('updated %s in DB' % thing)  # update value of thing in DB

    #  register thing and its io-ports in db
    elif thing not in THINGS and argument == 'report':
        report = json.loads(message)  # parse report msg in json
        THINGS[thing] = r.table('things').insert(report).run(spartakus)['generated_keys'][0]  # insert new thing in database and add ID
        print('created %s in DB' % thing)

    elif thing in THINGS and argument == 'report':
        r.table('things').get(THINGS[thing]).update({"status": 1}).run(spartakus)
        print('%s already in DB... nothing to do here' % thing)

    else:
        print('what happend inside callback_logic!? %s ' % thing)


#  create rules in db
#  expecting '{"time":"Szene1", "sender":{"name":"ESP1_POTI","id":"unique_id", "output":"poti"}, "receiver":{"name":"ESP1_LED","id":"unique_id", "input":"pwm"}}'
def callback_add_rule(thing, argument, message):
    rule = json.loads(message)
    # print(rule)
    try:
        if (rule['sender']['name'] and rule['receiver']['name']) in THINGS:  # besser in db checken
            if 'id' not in rule['sender']:
                rule['sender']['id'] = THINGS[rule['sender']['name']]
            if 'id' not in rule['receiver']:
                rule['receiver']['id'] = THINGS[rule['receiver']['name']]
            r.table('rules').insert(rule).run(spartakus)
            print('added rule for %s' % str(rule))

        else:
            print('not a thing to add a rule to')
    except Exception as e:
        print(e)


#  create or delete rules in db
def callback_remove_rule(thing, argument, message):
    rule = json.loads(message)

    try:
        if 'id' in rule:
            r.table('rules').get(rule['id']).delete().run(spartakus)
            print('removed rule for %s' % str(rule))
        elif (rule['sender']['name'] and rule['receiver']['name']) in THINGS:  # besser in db checken
            if 'id' not in rule['sender']:
                rule['sender']['id'] = THINGS[rule['sender']['name']]
            if 'id' not in rule['receiver']:
                rule['receiver']['id'] = THINGS[rule['receiver']['name']]
                r.table('rules').filter(rule).delete().run(spartakus)
                print('removed rule for %s' % str(rule))
        else:
            print('not a thing to remove rule from')
    except Exception as e:
        print('Exception callback_remove_rule')
        print(e)


#  create or delete rules in db
def callback_remove_thing(thing, argument, message):
    try:
        if message in THINGS:
            r.table('things').get(THINGS[message]).delete().run(spartakus)
            THINGS.pop(message, None)
            print('removed thing for %s' % str(message))
        else:
            print('not a thing to remove')
    except Exception as e:
        print('Exception callback_remove_thing')
        print(e)


#  create or delete rules in db
def callback_dump_all(thing, argument, message):
    for thing in r.db('spartakus').table('things').run():
        send_mqtt(TOPIC_OUT_DB + 'change/things/add', json.dumps(thing))
    for rules in r.db('spartakus').table('rules').run():
        send_mqtt(TOPIC_OUT_DB + 'change/rules/add', json.dumps(rules))


# send infos about own in- and outnodes
def callback_report(thing, argument, message):
    print('callback_report on: %s' % message)
    send_mqtt(TOPIC_OUT + 'report', REPORT_IO)  # send skill info on first connection to broker

# register all callbacks for different topics of Parent and Child things
mqtt_callback = {TOPIC_IN + 'report': callback_report,
                 TOPIC_PING: callback_report,
                 TOPIC_IN + 'rule/add': callback_add_rule,
                 TOPIC_IN + 'rule/remove': callback_remove_rule,
                 TOPIC_IN + 'thing/remove': callback_remove_thing,
                 TOPIC_IN + 'dump': callback_dump_all}


def on_connect(client, userdata, flags, rc):
    client.subscribe(TOPIC_IN + '#')  # subscribe to all incoming topics
    client.subscribe(TOPIC_PING)  # subscribe to all incoming topics
    client.subscribe(SUBSCRIBE_THINGS)  # subscribe to all outputs of things
    # send_mqtt(TOPIC_IN + 'report', 'send report on connect')

    # on startup ping everyone in system
    send_mqtt(TOPIC_PING, '1')


def on_message(client, userdata, msg):
    thing = get_thing(msg.topic)
    argument = get_argument(msg.topic)
    try:
        message = float(msg.payload.decode("utf-8"))
    except ValueError:
        message = msg.payload.decode("utf-8")
    callback = mqtt_callback.get(get_matching(msg.topic), callback_logic)
    callback(thing, argument, message)


client = mqtt.Client(CLIENTID)
client.on_connect = on_connect  # register callback for event
client.on_message = on_message  # register callback

client.connect(MQTTP_HOST, MQTTP_PORT, 60)  # connect to MQTT broker


##############################################################################
# DATABASE STUFF
##############################################################################

def on_db_connect(db):
    if not db:
        r.db_create('spartakus').run()
        r.db('spartakus').table_create('things').run()
        r.db('spartakus').table_create('rules').run()
        print('created new spartakus database')
    else:
        # TEMPORARY: to prevent BUG remove existing db and ask everyone to report in
        # load CeBIT DATABASE HERE!1!
        r.db_drop('spartakus').run()
        r.db_create('spartakus').run()
        r.db('spartakus').table_create('things').run()
        r.db('spartakus').table_create('rules').run()

        if not r.db('spartakus').table_list().contains('things').run():
            r.db('spartakus').table_create('things').run()
        if not r.db('spartakus').table_list().contains('rules').run():
            r.db('spartakus').table_create('rules').run()
        for thing in r.db('spartakus').table('things').run():
            THINGS[thing['name']] = thing['id']
            # print('found ', str(thing['name']))
            send_mqtt(TOPIC_OUT_DB + 'change/things/add', json.dumps(thing))
        for item in r.db('spartakus').table('things').pluck('name', 'id').run():  # filter(r.row['type'] == 'hardware')
            print('on db connect found thing: ', item)


r.connect(host=DB_HOST, port=DB_PORT).repl()  # connect to RETHINKDB database
on_db_connect(r.db_list().contains('spartakus').run())  # check for, or create a new database
spartakus = r.connect(host=DB_HOST, port=DB_PORT, db='spartakus')  # connect to SPARTAKUS database


##############################################################################
# DATABASE CHANGEFEED IN EXTRA THREAD
##############################################################################

# {'new_val': None, 'old_val': {'output': {'change': 'json', 'rule': 'json', 'report': 'json'}, 'input': {'change': 1, 'rule': 1, 'report': 1}, 'id': 'ab462a9c-76be-46d1-a40a-54d1242a79f4', 'name': 'LOGIC', 'type': 'logic', 'status': 1}}
def feed_to_mqtt(msg, table):
    new_val = msg.get('new_val')
    old_val = msg.get('old_val')
    if table == 'things':
        parse_things_feed(new_val, old_val)
    elif table == 'rules':
        parse_rules_feed(new_val, old_val)


def parse_rules_feed(new_val, old_val):
    # print(new_val, old_val)
    if new_val is None:
        send_mqtt(TOPIC_OUT_DB + 'change/rules/remove', json.dumps(old_val))
    elif old_val is None:
        send_mqtt(TOPIC_OUT_DB + 'change/rules/add', json.dumps(new_val))
    else:
        print('both not None in parse_rules_feed')


def parse_things_feed(new_val, old_val):
    # print(new_val, old_val)
    if new_val is None:
        send_mqtt(TOPIC_OUT_DB + 'change/things/remove', json.dumps(old_val))
    elif old_val is None:
        send_mqtt(TOPIC_OUT_DB + 'change/things/add', json.dumps(new_val))
    else:
        print('both not None check for value change')
        parse_value_feed(new_val, old_val)
        # print(new_val, old_val)


def parse_value_feed(new_val, old_val):
    for key in new_val:
        if old_val[key] != new_val[key]:
            print('diff key event: %s ' % new_val[key])
            try:
                for subkey in new_val[key]:
                    if old_val[key][subkey] != new_val[key][subkey]:
                        print('diff key event: %s ' % new_val[key][subkey])
                        msg = json.dumps({'name': new_val['name'], 'id': new_val['id'], 'output': {subkey: new_val[key][subkey]}})
                        send_mqtt(TOPIC_OUT_DB + 'change/value', msg)
            except Exception as e:
                print(e)


def get_changes(table_name):
    spartakus = r.connect(host=DB_HOST, port=DB_PORT, db='spartakus')  # connect to SPARTAKUS database

    for change in r.table(table_name).changes().run(spartakus):
        # stream_queue.put(change)
        feed_to_mqtt(change, table_name)

stream_queue = Queue()


##############################################################################
# WEBSOCKET CONNECTION
##############################################################################
'''
class MainHandler(tornado.web.RequestHandler):
    def get(self):
        loader = tornado.template.Loader(".")
        self.write(loader.load("index.html").generate())


class WSHandler(tornado.websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def open(self):
        print('websocket connection opened...')
        # self.write_message("server says 'hi'")

    def on_message(self, message):
        # self.write_message("The server says: " + message + " back at you")
        if (message == "init"):
            print('init requested')
            for item in r.table('things').run(spartakus):  # filter(r.row['type'] == 'hardware')
                print(item)

    def on_close(self):
        print('connection closed...')

application = tornado.web.Application([
    (r'/ws', WSHandler),
    (r'/', MainHandler),
    (r"/(.*)", tornado.web.StaticFileHandler, {"path": "./resources"}),
])
'''

##############################################################################
# MAIN LOOP
##############################################################################

if __name__ == "__main__":
    '''
    # auskommentiert, weil im Moment nicht benötigt
    application.listen(9090)
    tornado.ioloop.IOLoop.instance().start()
    '''
    # CHANGEFEED from DB as extra thread
    thread1 = Thread(target=get_changes, args=("things",))
    thread2 = Thread(target=get_changes, args=("rules",))
    thread1.setDaemon(True)
    thread2.setDaemon(True)
    thread1.start()
    thread2.start()

    try:
        while True:
            client.loop()
    except KeyboardInterrupt:
        r.table('things').get(THINGS[CLIENTID]).delete().run(spartakus)
        print('interrupted!')
    print('bye bye!')
